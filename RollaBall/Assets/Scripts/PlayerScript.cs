﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PlayerScript : MonoBehaviour
{
    private Rigidbody r;
    public float speed;
    private int score;
    public Text scoreText;
    public Text winText;
    // Start is called before the first frame update
    void Start()
    {
        r =GetComponent<Rigidbody>();
        score=0;
        setScore();
        winText.text="";
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void FixedUpdate()
    {
        float moveHorizontal =Input.GetAxis("Horizontal");
        float moveVertical =Input.GetAxis("Vertical");
        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        r.AddForce(movement*speed);
    }

    void OnTriggerEnter(Collider other){
        if(other.gameObject.CompareTag("Food"))
        {
            
            other.gameObject.SetActive(false);
            score++;
            setScore();
            if(score>=16){
                winText.text="YOU WIN";
                Time.timeScale=0;
            }
        }
    }

    void setScore(){
        scoreText.text = "Score: " + score.ToString();
    }
}
